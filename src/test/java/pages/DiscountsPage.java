package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class DiscountsPage {

    private final String ACTIVE_RATING_STARS = ".rating-item--active";
    public final SelenideElement productsElement = $("#insales-section-collection");
    public final ElementsCollection productList = productsElement.$$(".col-lg-4");
    public final ElementsCollection productTitles = productsElement.$$(".product_card-title");
    public final SelenideElement sortFilter = $x("//select[@class='js-filter-sort input--sort']");

    public void checkSortByPopularity(List<String> referenceSortedList) {
        sortFilter.selectOption("По популярности");
        productTitles.shouldHave(texts(referenceSortedList));
    }

    public void checkProductRatingIs(String name, int expectedRating) {
        productList
                .find(text(name))
                .findAll(ACTIVE_RATING_STARS)
                .shouldHave(size(expectedRating));
    }

}
