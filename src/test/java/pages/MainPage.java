package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class MainPage {

    private final String MAIN_PAGE_URL = "https://shop.1-ofd.ru/";

    private final SelenideElement upperMenu = $("#sticky-wrapper");
    private final ElementsCollection upperMenuSections = upperMenu.$$(".nav-item");

    public MainPage() {
        Selenide.open(MAIN_PAGE_URL);
    }

    public DiscountsPage clickDiscountPage() {
        upperMenuSections.find(text("Акция")).click();
        return new DiscountsPage();
    }

    public MainPage checkUpperMenuIsVisible() {
        upperMenu.shouldBe(visible);
        return this;
    }

    public void checkUpperMenuContainsAllSections(String sections) {
        upperMenu.shouldHave(text(sections));
    }
}
