package dataProviders;

import org.testng.annotations.DataProvider;

import java.util.List;

public class DiscountsPageDataProvider {

    @DataProvider(name = "menuSections")
    public static Object[][] getTest1TestData() {
        return new Object[][]{
                {"КАТАЛОГ\n" +
                "АКЦИЯ\n" +
                "ОПЛАТА\n" +
                "ДОСТАВКА\n" +
                "САМОВЫВОЗ\n" +
                "НОВОСТИ\n" +
                "КОНТАКТЫ\n" +
                "НАШИ КЛИЕНТЫ"}
        };
    }

    @DataProvider(name = "referencePopularitySortList")
    public static Object[][] getTest2TestData() {
        return new Object[][]{
                {
                    List.of(
                            "Эвотор 7.3",
                            "Эвотор 5",
                            "ОФД на 6 месяцев",
                            "ОФД на 12 месяцев",
                            "ОФД на 15 месяцев",
                            "Атол FPrint-22ПТК",
                            "Меркурий 185Ф",
                            "Фискальный накопитель на 15 месяцев",
                            "Фискальный накопитель на 36 месяцев",
                            "Весы Мидл МТ 15 МГЖА (2/5; 340*230) Базар 2.1",
                            "Весы Mercury M-ER 322CP",
                            "Весы Масса-К МК-15.2-Т21",
                            "ПТК MSPOS-Е Ф"
                )
                }};
    }

    @DataProvider(name = "productName")
    public static Object[][] getTest3TestData() {
        return new Object[][]{
                {"Эвотор 5", 5}
        };
    }
}
