package tests;

import org.testng.annotations.AfterTest;

import static com.codeborne.selenide.Selenide.closeWebDriver;

public abstract class BaseTest {

    @AfterTest
    public void tearDown() {
        closeWebDriver();
    }
}
