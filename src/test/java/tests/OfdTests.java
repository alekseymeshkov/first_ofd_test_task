package tests;

import dataProviders.DiscountsPageDataProvider;
import org.testng.annotations.Test;
import pages.MainPage;

import java.util.List;

public class OfdTests extends BaseTest {

    @Test(dataProvider = "menuSections", dataProviderClass = DiscountsPageDataProvider.class)
    public void test1(String menuSections) {
        new MainPage()
                .checkUpperMenuIsVisible()
                .checkUpperMenuContainsAllSections(menuSections);
    }

    @Test(dataProvider = "referencePopularitySortList", dataProviderClass = DiscountsPageDataProvider.class)
    public void test2(List<String> referencelist) {
        new MainPage()
                .clickDiscountPage()
                .checkSortByPopularity(referencelist);
    }

    @Test(dataProvider = "productName", dataProviderClass = DiscountsPageDataProvider.class)
    public void test3(String productName, int rating) {
        new MainPage()
                .clickDiscountPage()
                .checkProductRatingIs(productName, rating);
    }
}
